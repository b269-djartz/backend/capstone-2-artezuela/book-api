const Book = require("../models/Book");
const User = require("../models/User");
const Transaction = require("../models/Transaction");

//<<-------------CREATE PRODUCT (ADMIN)---------------->>\\
module.exports.addBook = async (data) => {
	// Check if the user is an admin
	if (data.isAdmin) {
		// Create a new Book object using the Book model and the data provided
		const existingBook = await Book.findOne({ title: data.book.title });
      if (existingBook) {
        return { success: false, message: "Book with the same title already exists!" };
      }

    const newBook = new Book({
			title: data.book.title,
			genre: data.book.genre,
			author: data.book.author,
			description : data.book.description,
			price: data.book.price,
			stock: data.book.stock,
			tags: data.book.tags,
		});
		// Save the new book to the database and return a Promise
		//The `book` parameter in the callback represents the saved instance of the book object and `error`` parameter represents any errors that might occur during the save operation.
		await newBook.save();
		return true
	};

	// If the user is not an admin, return a Promise with a message
	const message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	});
};

//<<-------------SUGGEST A BOOK---------------->>\\
module.exports.suggestAvaialableBooks = async (data) => {
	
	const sortOption = {};

	switch (data.sort) {
	  case "highestPrice":
	    sortOption.price = -1;
	    break;
	  case "lowestPrice":
	    sortOption.price = 1;
	    break;
	  case "highestSold":
	    sortOption.sold = -1;
	    break;
	  case "lowestSold":
	    sortOption.sold = 1;
	    break;
    case "highestStar":
      sortOption.starRating = -1;
      break;
    case "lowestStar":
      sortOption.starRating = 1;
      break;
    case "highestRatings":
      sortOption.ratings = -1;
      break;
    case "lowestStar":
      sortOption.ratings = 1;
      break;
	  case "titleAZ":
	    sortOption.title = 1;
	    break;
	  case "titleZA":
	    sortOption.title = -1;
	    break;
	  default:
	    break;
	}

	const books = await Book.find({ tags: { $in: [data.reqBody.option2,data.reqBody.option3] },genre: data.reqBody.option1,stock: { $gt: 0 } }).sort(sortOption);

  	if (books.length === 0) {
    	return "Result not found";
  	}

	const updatedBooks = [];

	for (const book of books) {
	const users = await User.find({ _id: data.userId, wishlist: book.title });

	if (users.length > 0) {
      updatedBooks.push({
        _id: book._id,
        title: book.title,
        genre: book.genre,
        author: book.author,
        price: book.price,
        starRating: book.starRating,
        ratings: book.ratings,
        stock: book.stock,
        sold: book.sold,
        inWishlist: true,
        createdOn: book.createdOn,
      });
    } else {
      updatedBooks.push({
        _id: book._id,
        title: book.title,
        genre: book.genre,
        author: book.author,
        price: book.price,
        starRating: book.starRating,
        ratings: book.ratings,
        stock: book.stock,
        sold: book.sold,
        createdOn: book.createdOn,
      });
	}
	}

	return updatedBooks;
};

//<<-------------RETRIEVE ALL ACTIVE PRODUCTS---------------->>\\
module.exports.getAllAvailableBooks = async (data) => {
  const sortOption = {};

  switch (data.sort) {
    case "highestPrice":
      sortOption.price = -1;
      break;
    case "lowestPrice":
      sortOption.price = 1;
      break;
    case "highestSold":
      sortOption.sold = -1;
      break;
    case "lowestSold":
      sortOption.sold = 1;
      break;
    case "highestStar":
      sortOption.starRating = -1;
      break;
    case "lowestStar":
      sortOption.starRating = 1;
      break;
    case "highestRatings":
      sortOption.ratings = -1;
      break;
    case "lowestStar":
      sortOption.ratings = 1;
      break;
    case "titleAZ":
      sortOption.title = 1;
      break;
    case "titleZA":
      sortOption.title = -1;
      break;
    default:
      break;
  }

  const books = await Book.find({ stock: { $gt: 0 } }).sort(sortOption);

  if (books.length === 0) {
    return "Result not found";
  }

  const updatedBooks = [];

  for (const book of books) {
    const users = await User.find({ _id: data.userId, wishlist: book.title });

    if (users.length > 0) {
      updatedBooks.push({
        _id: book._id,
        title: book.title,
        genre: book.genre,
        author: book.author,
        price: book.price,
        starRating: book.starRating,
        ratings: book.ratings,
        stock: book.stock,
        sold: book.sold,
        inWishlist: true,
        createdOn: book.createdOn,
      });
    } else {
      updatedBooks.push({
        _id: book._id,
        title: book.title,
        genre: book.genre,
        author: book.author,
        price: book.price,
        starRating: book.starRating,
        ratings: book.ratings,
        stock: book.stock,
        sold: book.sold,
        createdOn: book.createdOn,
      });
    }
  }

  return updatedBooks; 
};

//<<-------------RETRIEVE ALL ACTIVE ROMANCE BOOKS---------------->>\\
module.exports.getRomanceAvaialableBooks = async (data) => {
 	
 	const sortOption = {};

 	switch (data.sort) {
    case "highestPrice":
      sortOption.price = -1;
      break;
    case "lowestPrice":
      sortOption.price = 1;
      break;
    case "highestSold":
      sortOption.sold = -1;
      break;
    case "lowestSold":
      sortOption.sold = 1;
      break;
    case "highestStar":
      sortOption.starRating = -1;
      break;
    case "lowestStar":
      sortOption.starRating = 1;
      break;
    case "highestRatings":
      sortOption.ratings = -1;
      break;
    case "lowestStar":
      sortOption.ratings = 1;
      break;
    case "titleAZ":
      sortOption.title = 1;
      break;
    case "titleZA":
      sortOption.title = -1;
      break;
    default:
      break;
  }

 	const books = await Book.find({ stock: { $gt: 0 }, genre: "Romance" }).sort(sortOption);

  	if (books.length === 0) {
    	return "Result not found";
  	}

  const updatedBooks = [];

  for (const book of books) {
    const users = await User.find({ _id: data.userId, wishlist: book.title });

    if (users.length > 0) {
      updatedBooks.push({
        _id: book._id,
        title: book.title,
        genre: book.genre,
        author: book.author,
        price: book.price,
        starRating: book.starRating,
        ratings: book.ratings,
        stock: book.stock,
        sold: book.sold,
        inWishlist: true,
        createdOn: book.createdOn,
      });
    } else {
      updatedBooks.push({
        _id: book._id,
        title: book.title,
        genre: book.genre,
        author: book.author,
        price: book.price,
        starRating: book.starRating,
        ratings: book.ratings,
        stock: book.stock,
        sold: book.sold,
        createdOn: book.createdOn,
      });
    }
  }

  return updatedBooks;
};

//<<-------------RETRIEVE ALL ACTIVE SELF_HELP BOOKS---------------->>\\
module.exports.getSelfHelpAvaialableBooks = async (data) => {

	const sortOption = {};

	switch (data.sort) {
    case "highestPrice":
      sortOption.price = -1;
      break;
    case "lowestPrice":
      sortOption.price = 1;
      break;
    case "highestSold":
      sortOption.sold = -1;
      break;
    case "lowestSold":
      sortOption.sold = 1;
      break;
    case "highestStar":
      sortOption.starRating = -1;
      break;
    case "lowestStar":
      sortOption.starRating = 1;
      break;
    case "highestRatings":
      sortOption.ratings = -1;
      break;
    case "lowestStar":
      sortOption.ratings = 1;
      break;
    case "titleAZ":
      sortOption.title = 1;
      break;
    case "titleZA":
      sortOption.title = -1;
      break;
    default:
      break;
  }

	const books = await Book.find({ stock: { $gt: 0 },genre: "Self-help" }).sort(sortOption);

  	if (books.length === 0) {
    	return "Result not found";
  	}

	const updatedBooks = [];

	for (const book of books) {
	const users = await User.find({ _id: data.userId, wishlist: book.title });

	if (users.length > 0) {
      updatedBooks.push({
        _id: book._id,
        title: book.title,
        genre: book.genre,
        author: book.author,
        price: book.price,
        starRating: book.starRating,
        ratings: book.ratings,
        stock: book.stock,
        sold: book.sold,
        inWishlist: true,
        createdOn: book.createdOn,
      });
    } else {
      updatedBooks.push({
        _id: book._id,
        title: book.title,
        genre: book.genre,
        author: book.author,
        price: book.price,
        starRating: book.starRating,
        ratings: book.ratings,
        stock: book.stock,
        sold: book.sold,
        createdOn: book.createdOn,
      });
	}
	}

	return updatedBooks;
};

//<<-------------RETRIEVE ALL ACTIVE FANTASY BOOKS---------------->>\\
module.exports.getFantasyAvaialableBooks = async (data) => {
	
  const sortOption = {};
  
	switch (data.sort) {
    case "highestPrice":
      sortOption.price = -1;
      break;
    case "lowestPrice":
      sortOption.price = 1;
      break;
    case "highestSold":
      sortOption.sold = -1;
      break;
    case "lowestSold":
      sortOption.sold = 1;
      break;
    case "highestStar":
      sortOption.starRating = -1;
      break;
    case "lowestStar":
      sortOption.starRating = 1;
      break;
    case "highestRatings":
      sortOption.ratings = -1;
      break;
    case "lowestStar":
      sortOption.ratings = 1;
      break;
    case "titleAZ":
      sortOption.title = 1;
      break;
    case "titleZA":
      sortOption.title = -1;
      break;
    default:
      break;
  }

	const books = await Book.find({ stock: { $gt: 0 },genre: "Fantasy" }).sort(sortOption);

  	if (books.length === 0) {
    	return "Result not found";
  	}

	const updatedBooks = [];

	for (const book of books) {
	const users = await User.find({ _id: data.userId, wishlist: book.title });

	if (users.length > 0) {
      updatedBooks.push({
        _id: book._id,
        title: book.title,
        genre: book.genre,
        author: book.author,
        price: book.price,
        starRating: book.starRating,
        ratings: book.ratings,
        stock: book.stock,
        sold: book.sold,
        inWishlist: true,
        createdOn: book.createdOn,
      });
    } else {
      updatedBooks.push({
        _id: book._id,
        title: book.title,
        genre: book.genre,
        author: book.author,
        price: book.price,
        starRating: book.starRating,
        ratings: book.ratings,
        stock: book.stock,
        sold: book.sold,
        createdOn: book.createdOn,
      });
	}
	}

	return updatedBooks;
};

//<<-------------RETRIEVE SINGLE PRODUCT---------------->>\\
module.exports.getBook = async (data) => {
	const book =  await Book.findById(data.reqParams.bookId)
	if (!book) {
    	return "Book not found";
  	}
	const updatedBooks = [];

	const users = await User.find({ _id: data.userId, wishlist: book.title });

	if (users.length > 0) {
      updatedBooks.push({
        _id: book._id,
        title: book.title,
        genre: book.genre,
        author: book.author,
        description: book.description,
        price: book.price,
        starRating: book.starRating,
        ratings: book.ratings,
        stock: book.stock,
        sold: book.sold,
        inWishlist: true,
        createdOn: book.createdOn,
        reviews: book.reviews,
      });
    } else {
      updatedBooks.push({
        _id: book._id,
        title: book.title,
        genre: book.genre,
        author: book.author,
        description: book.description,
        price: book.price,
        starRating: book.starRating,
        ratings: book.ratings,
        stock: book.stock,
        sold: book.sold,
        createdOn: book.createdOn,
        reviews: book.reviews,
      });
	}

	return updatedBooks;
};

//<<-------------UPDATE SINGLE PRODUCT (ADMIN)---------------->>\\
module.exports.updateBook = async (data) => {
	if(data.isAdmin) {

  const existingTitle = await Book.findOne({title: data.reqBody.title})
  if(existingTitle) {
    return "Title already exists"
  }
	const updatedBook = {
	title: data.reqBody.title,
	genre: data.reqBody.genre,
	author: data.reqBody.author,
	description: data.reqBody.description,
	price: data.reqBody.price,
  stock: data.reqBody.stock,
  tags: data.reqBody.tags
	};
	return Book.findByIdAndUpdate(data.reqParams.bookId, updatedBook).then((book, error) => {
		if(error) {
			return false;
		} else {
			return true
		};
	});
	}
	const message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	});
	
};

//<<-------------ARCHIVE PRODUCT (ADMIN)---------------->>\\
module.exports.archiveBook = (data) => {
	if(data.isAdmin) {
		const updateInStockField = {
		stock: 0
	};
	return Book.findByIdAndUpdate(data.reqParams.bookId, updateInStockField).then((course, error) => {
		if (error) {
			return false;
		} else {
			return true;
		};
	});
	}
	const message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	});
	
};

//<<-------------ADD REVIEW ---------------->>\\
module.exports.addReview = async (data) => {
  const book = await Book.findById(data.reqParams.bookId);
  if (!book) {
    return "Id not found";
  }
  const user = await User.findById(data.userId);
  if (!user) {
    return "User not found";
  }
  
  // Check if user has already left a review
  const existingReview = book.reviews.find((review) => {
    return review.username === user.username;
  });
  if (existingReview) {
    return "User has already left a review for this book";
  }
  
  // Check if user has purchased the book and it has been delivered
  const transaction = await Transaction.findOne({
    userId: data.userId,
    "books.title": book.title,
    status: "Delivered"
  });
  if (!transaction) {
    return "You cannot leave a review for this book until you purchased it and has been delivered to you";
  }
  
  const review = {
    username: user.username,
    name: user.name,
    star: data.reqBody.star,
    comment: data.reqBody.comment,
  };
  book.reviews.push(review);
  book.ratings = book.reviews.length;

  // Calculate average star rating
  let totalStars = 0;
  for (let i = 0; i < book.reviews.length; i++) {
    totalStars += book.reviews[i].star;
  }
  book.starRating = Number((totalStars / book.ratings).toFixed(2))

  return book.save().then((thebook, error) => {
    if (error) {
      return false;
    } else {
      return true;
    }
  });
};

//<<-------------EDIT REVIEW ---------------->>\\
module.exports.editReview = async (data) => {
  const book = await Book.findById(data.reqParams.bookId);
  if (!book) {
    return "Book not found";
  }

  const user = await User.findById(data.userId);
  if (!user) {
    return "User not found";
  }

  const reviewIndex = book.reviews.findIndex((review) => review.username === user.username);
  if (reviewIndex < 0) {
    return "Review not found";
  }

  book.reviews[reviewIndex].star = data.reqBody.star;
  book.reviews[reviewIndex].comment = data.reqBody.comment;

  // Recalculate star rating
  let totalStars = 0;
  for (let i = 0; i < book.reviews.length; i++) {
    totalStars += book.reviews[i].star;
  }
  book.starRating = Number((totalStars / book.ratings).toFixed(2))

  return book.save().then((book, error) => {
    if (error) {
      return error;
    } else {
      return "Review updated successfully";
    }
  });
};

//<<-------------DELETE REVIEW ---------------->>\\
module.exports.deleteReview = async (data) => {
  const book = await Book.findById(data.reqParams.bookId);
  if (!book) {
    return "Book not found";
  }

  const user = await User.findById(data.userId);
  if (!user) {
    return "User not found";
  }

  const reviewIndex = book.reviews.findIndex(review => review.username === user.username);
  if (reviewIndex < 0) {
    return "Review not found";
  }

  book.reviews.splice(reviewIndex, 1);
  book.ratings = book.reviews.length;

  // Calculate average star rating
  if (book.ratings > 0) {
    let totalStars = 0;
    for (let i = 0; i < book.reviews.length; i++) {
      totalStars += book.reviews[i].star;
    }
    book.starRating = Number((totalStars / book.ratings).toFixed(2))
  } else {
    book.starRating = 0;
  }

  return book.save().then((book,error) => {
    if (error){
      return error
    } else {
      return "Review deleted successfully";
    }
  });
};

//<<-------------ON SALE ALL ITEMS ---------------->>\\
module.exports.onSale = async (data) => {
   if(data.isAdmin){
   	const books = await Book.find();

    books.forEach(async book => {
      book.price *= (1-data.reqBody.percentage);
      await book.save();
    });
    return `Prices reduced by ${data.reqBody.percentage}`;
   }
   const message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	}); 
};

//<<-------------ON SALE ITEM BY GENRE---------------->>\\
module.exports.onSaleByGenre = async (data) => {
    if(data.isAdmin) {
    const books = await Book.find({ genre: data.reqBody.genre });

    books.forEach(async book => {
      book.price *= (1-data.reqBody.percentage);
      await book.save();
    });
    return `Prices reduced by ${data.reqBody.percentage} for all books in the ${data.reqBody.genre} genre`;	
    }
    const message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	}); 
};

//<<-------------ADD TO WISHLIST---------------->>\\
module.exports.addToWishlist = async (data) => {
    const user = await User.findById(data.userId);
    if (!user) {
      return 'User not found';
    }

    const book = await Book.findById(data.reqParams.bookId);
    if (!book) {
      return 'Book not found';
    }

    if (user.wishlist.includes(book.title)) {
      return 'Book already in wishlist';
    }

    user.wishlist.push(book.title);
    await user.save();


    return 'Book added to wishlist';
};

//<<-------------SEARCH BOOK---------------->>\\
module.exports.searchBooks = async (reqBody) => {
  const query = reqBody.query;

  const books = await Book.find({
    $or: [
      { title: { $regex: query, $options: "i" } },
      { genre: { $regex: query, $options: "i" } },
      { author: { $regex: query, $options: "i" } },
      { description: { $regex: query, $options: "i" } },
      { author: { $regex: query, $options: "i" } }
    ]
  });

  if (books.length === 0) {
    return { error: "No books found" };
  }

  return books;
}




