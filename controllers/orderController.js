const Order = require("../models/Order")
const Book = require("../models/Book")
const User = require("../models/User")
const Transaction = require("../models/Transaction")
const Voucher = require("../models/Voucher")
const auth = require("../auth");


//<<-------------ADD TO CART---------------->>\\
module.exports.addToCart = async (data) => {

  if(data.isAdmin){
    return "Admins are restricted in this area"
  }

  const book = await Book.findById(data.reqBody.bookId);
  if (!book) {
    return `Book with ID ${data.reqBody.bookId} not found`;
  }

  const requestedQuantity = data.reqBody.quantity;
  if (book.stock < requestedQuantity) {
    return `The quantity you are requesting is more than the amount of stock that is available`;
  }

  // Check if the user already has an order with the same book ID
  let existingOrder = await Order.findOne({
    userId: data.userId,
    "books.bookId": data.reqBody.bookId,
    status: "Added to cart"
  });

  if (existingOrder) {
    // If an order with the same book ID already exists, update the quantity
    const existingQuantity = existingOrder.books[0].quantity;
    const newQuantity = existingQuantity + requestedQuantity;
    if (book.stock < newQuantity) {
      return `Stock exceeds request quantity`;
    }

    existingOrder.books[0].quantity = newQuantity;
    existingOrder.subtotal = book.price * newQuantity;
    await existingOrder.save();

  } else {
    // If no order with the same book ID exists, create a new one
    const subtotal = book.price * requestedQuantity;
    const newOrder = new Order({
      userId: data.userId,
      books: [
        {
          bookId: data.reqBody.bookId,
          title: book.title,
          quantity: requestedQuantity
        }
      ],  
      subtotal: subtotal
    });
    await newOrder.save();
  }

  return "Added to cart!";
};

//<<-------------VIEW MY CART--------------->>\\
module.exports.myCart = (userData) => {
	return Order.find({status: "Added to cart", userId: userData},{status:0,userId:0}).then((result) => { 
		return result
	});
}

//<<-------------CHANGE QUANTITY--------------->>\\
module.exports.changeQuantity = async (reqParams, reqBody) => {
  // Check if the requested quantity is 0
  if (reqBody.quantity === 0) {
    // Delete the order if the quantity is 0
    const deletedOrder = await Order.findByIdAndDelete(reqParams.orderId);
    if (deletedOrder) {
      // Return true if the order was deleted successfully
      return "Order deleted";
    } else {
      // Return false if the order could not be deleted
      return "Order could not be deleted";
    }
  } else {
    // Find the order that needs to be updated
    const order = await Order.findById(reqParams.orderId);
    if (!order) {
      // Return false if the order cannot be found
      return "Order cant be found";
    }

    // Find the book associated with the order
    const book = await Book.findById(order.books[0].bookId);
    if (!book) {
      // Return false if the book cannot be found
      return "Book cannot be found";
    }

    // Check if the requested quantity exceeds the book's stock
    if (reqBody.quantity > book.stock) {
      // Return false if the requested quantity exceeds the book's stock
      return "requested quantity exceeds the book's stock";
    }

    // Update the order with the new quantity
    const updatedOrder = await Order.findOneAndUpdate(
      { _id: reqParams.orderId, "books.bookId": order.books[0].bookId },
      { $set: { "books.$.quantity": reqBody.quantity } },
      { new: true }
    );
    if (updatedOrder) {
      // Return true if the order was updated successfully
      return true;
    } else {
      // Return false if the order could not be updated
      return "Order could not be updated";
    }
  }
};

//<<-------------REMOVE PRODUCT FROM CART--------------->>\\
module.exports.removeProduct = (reqParams) => {
	return Order.findByIdAndDelete(reqParams.orderId).then((course, error) => {
		if (error) {
			return false;
		} else {
			return true;
		};
	});
	
};

//<<-------------TOTAL AMOUNT IN CART--------------->>\\
module.exports.totalAmount = async (data) => {
  const user = await User.findById(data.userData);
  if (!user) {
    return "User not found";
  }

  const location = user.location.toLowerCase();
  let shippingFee = 0;
  if (location === "visayas") {
    shippingFee = 150;
  } else if (location === "luzon") {
    shippingFee = 100;
  } else if (location === "mindanao") {
    shippingFee = 170;
  }

  const orders = await Order.aggregate([
    {
      $match: { userId: data.userData, status: "Added to cart" }
    },
    {
      $group: {
        _id: "$userId",
        subtotal: { $sum: "$subtotal" }
      }
    }
  ]);

  const orderQuantity = await Order.aggregate([
    {
      $match: { userId: data.userData, status: "Added to cart" }
    },
    {
      $unwind: "$books"
    },
    {
      $group: {
        _id: "$userId",
        totalQuantity: { $sum: "$books.quantity" }
      }
    }
  ]);


  let voucherDiscount = 0;
  if (data.reqBody.voucher) {
    const voucher = await Voucher.findOne({ voucherName: data.reqBody.voucher });
    if (voucher) {
      voucherDiscount = voucher.discount;
    } else {
      return "Invalid Voucher"
    }
  }

  if (orders.length > 0) {

    const subtotal = orders[0].subtotal;
    const totalQuantity = orderQuantity[0].totalQuantity;
    let shippingFeeIncrease = 0;
    if (totalQuantity > 5 && totalQuantity <= 10) {
      shippingFeeIncrease = 150;
    } else if (totalQuantity > 10 && totalQuantity <= 20) {
      shippingFeeIncrease = 350;
    }
    shippingFee += shippingFeeIncrease;
    const total = subtotal + shippingFee - voucherDiscount;
    return `Your total amount will be:
              
            subtotal: ${subtotal}
                             +
         shippingFee: ${shippingFee}
                             -
     voucherDiscount: ${voucherDiscount}

     ------------------------------------------
         totalAmount: ${total}
            `;
  } else {
    return `Add to cart first `;
  }
};


//<<-------------CHECKOUT--------------->>\\
module.exports.checkout = async (data) => {

  // Check if the user has any orders to checkout
  const orders = await Order.find({userId:data.userData, status: "Added to cart"});
  if(orders.length === 0) {
      return "No orders to checkout";
  }

  // Calculate the total amount and subtotal by iterating over each order and adding up the subtotals of all orders
  let totalAmount = 0;
  let subtotal = 0;
  for(const order of orders) {
      subtotal += order.subtotal;
      totalAmount += order.subtotal;
  }

  // Add shipping fee and voucher discount to the total amount, if applicable
  const user = await User.findById(data.userData);
  if (!user) {
      return "User not found";
  }


  const location = user.location.toLowerCase();
  let shippingFee = 0;
  if (location === "visayas") {
      shippingFee = 150;
  } else if (location === "luzon") {
      shippingFee = 100;
  } else if (location === "mindanao") {
      shippingFee = 170;
  }

  const orderQuantity = await Order.aggregate([
    {
      $match: { userId: data.userData, status: "Added to cart" }
    },
    {
      $unwind: "$books"
    },
    {
      $group: {
        _id: "$userId",
        totalQuantity: { $sum: "$books.quantity" }
      }
    }
  ]);

  const totalQuantity = orderQuantity[0].totalQuantity;
  let shippingFeeIncrease = 0;
  if (totalQuantity > 5 && totalQuantity <= 10) {
    shippingFeeIncrease = 150;
  } else if (totalQuantity > 10 && totalQuantity <= 20) {
    shippingFeeIncrease = 350;
  }
  shippingFee += shippingFeeIncrease;

  totalAmount += shippingFee;

  let voucherDiscount = 0;
  if (data.reqBody.voucher) {
    const voucher = await Voucher.findOne({ voucherName: data.reqBody.voucher });
    if (voucher) {
      voucherDiscount = voucher.discount;

      // Update voucher database
      voucher.numberOfVoucherUse += 1;
      voucher.commision = 50 * voucher.numberOfVoucherUse;
      await voucher.save();
    } else {
      "Invalid voucher"
    }
  }
  
  totalAmount -= voucherDiscount;

  // Check payment method
  let paymentMethod = data.reqBody.paymentMethod;
  if (paymentMethod !== "CashOnDelivery" && paymentMethod !== "E-wallet") {
      return "Invalid payment method";
  }

  if (paymentMethod === "E-wallet") {
      const paymentAmount = data.reqBody.paymentAmount;
      if (!paymentAmount || paymentAmount < totalAmount) {
          return "Invalid payment amount";
      }
  }

  // Iterate over each order and update the stock of the corresponding book
  for(const order of orders) {
      for(const bookOrder of order.books) {
          const book = await Book.findById(bookOrder.bookId);
          if (!book) {
              return `Book with ID ${bookOrder.bookId} not found`;
          }
          const orderQuantity = bookOrder.quantity;
          if(book.stock < orderQuantity) {
              return `The stock of ${book.title} is less than the order quantity`;
          }
          book.stock -= orderQuantity;
          book.sold += orderQuantity; // increment sold field by orderQuantity
          await book.save();

          // Remove book from user's wishlist
          const index = user.wishlist.indexOf(book.title);
          if (index > -1) {
            user.wishlist.splice(index, 1);
            await user.save();
          }
      }
  }

  // Update the status and total amount of all orders to "Checked out"
  await Order.updateMany({userId: data.userData, status: "Added to cart"}, {status: "Checked out", totalAmount: totalAmount});

  // Create a new transaction record for the user
  const books = [];
  for(const order of orders) {
      for(const bookOrder of order.books) {
          const book = await Book.findById(bookOrder.bookId);
          if (!book) {
              return `Book with ID ${bookOrder.bookId} not found`;
          }
          books.push({
              title: book.title,
              quantity: bookOrder.quantity,
              price: book.price
          });

          // Remove book from book's wishlist
          book.inWishlist = false;
          await book.save();
      }
  }
  const newTransaction = new Transaction({
      userId: data.userData,
      totalAmount: totalAmount,
      subtotal: subtotal,
      shippingFee: shippingFee,
      voucherDiscount: voucherDiscount,
      status: "Pending",
      books: books,
      paymentMethod: paymentMethod
  });
 

await newTransaction.save();

return "Checkout successful";

};




































// code for deleting the cart if quanity will be changed to 0
/*
module.exports.changeQuantity = async (reqParams, reqBody) => {
  if (reqBody.quantity === 0) {
    const deletedOrder = await Order.findByIdAndDelete(reqParams.orderId);
    if (deletedOrder) {
      return true;
    } else {
      return false;
    }
  } else {
    const order = await Order.findById(reqParams.orderId);
    if (!order) {
      return false;
    }

    const book = await Book.findById(order.books[0].bookId);
    if (!book) {
      return false;
    }

    if (reqBody.quantity > book.stock) {
      return false;
    }

    const updatedOrder = await Order.findByIdAndUpdate(
      reqParams.orderId,
      { $set: reqBody },
      { new: true }
    );
    if (updatedOrder) {
      return true;
    } else {
      return false;
    }
  }
};

*/


/*module.exports.checkout = async (data) => {
	
	// Check if the user has any orders to checkout
	const orders = await Order.find({userId:data.userData, status: "Added to cart"});
	if(orders.length === 0) {
		return "No orders to checkout";
	}
	
	// Iterate over each order and update the stock of the corresponding book
	for(const order of orders) {
		const book = await Book.findById(order.books[0].bookId);
		const orderQuantity = order.books[0].quantity;
		if(book.stock < orderQuantity) {
			return `The stock of ${book.name} is less than the order quantity`;
		}
		book.stock -= orderQuantity;
		await book.save();
	}
	
	// Update the status of all orders to "Checked out"
	await Order.updateMany({userId: data.userData, status: "Added to cart"}, {status: "Checked out"});
	
	return "Checkout successful";
};*/



//transaction
/*
module.exports.checkout = async (data) => {
	
	// Check if the user has any orders to checkout
	const orders = await Order.find({userId: data.userData, status: "Added to cart"});
	if(orders.length === 0) {
		return "No orders to checkout";
	}
	
	// Compute the total amount including shipping fee and voucher discount
	const totalAmount = await module.exports.totalAmount(data);
	
	// Iterate over each order and update the stock of the corresponding book
	for(const order of orders) {
		const book = await Book.findById(order.books[0].bookId);
		const orderQuantity = order.books[0].quantity;
		if(book.stock < orderQuantity) {
			return `The stock of ${book.name} is less than the order quantity`;
		}
		book.stock -= orderQuantity;
		await book.save();
	}
	
	// Update the status and total amount of all orders to "Checked out"
	await Order.updateMany({userId: data.userData, status: "Added to cart"}, {status: "Checked out", totalAmount: totalAmount});
	
	// Create a new transaction record for the user
	const newTransaction = new Transaction({
		userId: data.userData,
		totalAmount: totalAmount,
		status: "Success"
	});
	await newTransaction.save();
	
	return "Checkout successful";
};
*/