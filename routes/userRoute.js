const express = require("express");
const router = express.Router();
const auth = require("../auth");
const userController = require("../controllers/userController");


//<<-------------REGISTER---------------->>\\
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

//<<-------------USER AUTHENTICATION---------------->>\\
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

//<<-------------RETRIEVE USER DETAILS---------------->>\\
router.post("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});

//<<-------------SET USER AS ADMIN---------------->>\\
router.patch("/:userId/setAsAdmin", auth.verify, (req, res) => {
	const data = {
		reqParams: req.params,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	userController.setUserAsAdmin(data).then(resultFromController => res.send(resultFromController));
});

//<<-------------RETRIEVE USER TRANSACTIONS---------------->>\\
router.post("/transactions", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
userController.getUserTransactions({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});

//<<-------------RETRIEVE USER PENDING TRANSACTION---------------->>\\
router.post("/transactions/pending", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
userController.getUserPendingTransactions({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});

//<<-------------RETRIEVE USER SHIPPED OUT TRANSACTION---------------->>\\
router.post("/transactions/shippedOut", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
userController.getUserShippedOutTransactions({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});

//<<-------------RETRIEVE USER DELIVERED TRANSACTION---------------->>\\
router.post("/transactions/delivered", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
userController.getUserDeliveredTransactions({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});

//<<-------------RETRIEVE USER CANCELLED TRANSACTION---------------->>\\
router.post("/transactions/cancelled", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
userController.getUserCancelledTransactions({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});

//<<-------------CANCEL USER TRANSACTION---------------->>\\
router.post("/cancelTransactions", auth.verify, (req, res) => {
userController.cancelTransaction(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/createVoucher", auth.verify, (req, res) => {
	const data = {
		voucher: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	userController.addVoucher(data).then(resultFromController => res.send(resultFromController));
});


router.get("/viewVoucher", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	userController.viewVoucher(data).then(resultFromController => res.send(resultFromController));
});

module.exports = router;





































/*router.get("/:userId/orders",auth.verify, (req, res) => {
	userController.getOrders(req.params).then(resultFromController => res.send(resultFromController));
});

router.get("/allOrders", (req, res) => {
	const data = {
		reqOrder: {},
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	userController.getAllOrders(data).then(resultFromController => res.send(resultFromController));
});*/

/*router.post("/checkout",auth.verify, (req,res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id, 
		bookId: req.body.bookId
	}
	userController.checkout(data).then(resultFromController => res.send(resultFromController))
})
*/



